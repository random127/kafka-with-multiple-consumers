package com.visiontotale.kafkawithmultipleconsumers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaWithMultipleConsumersApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaWithMultipleConsumersApplication.class, args);
	}

}
