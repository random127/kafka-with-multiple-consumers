package com.visiontotale.kafkawithmultipleconsumers.controller;

import com.visiontotale.kafkawithmultipleconsumers.model.User;
import com.visiontotale.kafkawithmultipleconsumers.service.KafKaProducerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/producer")
@RequiredArgsConstructor
public class ProducerController {

    private final KafKaProducerService kafKaProducerService;

    @PostMapping("/publishSimple")
    public ResponseEntity<?> sendSimpleMessage(@RequestParam("message") String message) {
        kafKaProducerService.sendSimpleMessage(message);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/publishUser")
    public ResponseEntity<?> sendUserMessage(@RequestBody User message) {
        kafKaProducerService.sendUserMessage(message);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
