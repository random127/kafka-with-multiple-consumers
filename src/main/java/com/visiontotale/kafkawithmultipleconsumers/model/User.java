package com.visiontotale.kafkawithmultipleconsumers.model;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private long userId;
    private String firstName;
    private String lastName;
}
