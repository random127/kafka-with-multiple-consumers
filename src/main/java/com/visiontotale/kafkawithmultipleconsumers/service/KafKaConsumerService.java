package com.visiontotale.kafkawithmultipleconsumers.service;

import com.visiontotale.kafkawithmultipleconsumers.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafKaConsumerService {

    @KafkaListener(topics = "${simple.topic.name}", groupId = "${simple.topic.group.id}")  // Marks this method to be the target of a Kafka message listener on the specified topics
    public void consume(String message) {
        log.info(String.format("Message recieved -> %s", message));
    }

    @KafkaListener(topics = "${user.topic.name}", groupId = "${user.topic.group.id}", containerFactory = "userKafkaListenerContainerFactory")  // Also marks this method to be the target of a Kafka message listener on the specified topics
    public void consume(User user) {
        log.info(String.format("User created -> %s", user));
    }
}
