package com.visiontotale.kafkawithmultipleconsumers.service;

import com.visiontotale.kafkawithmultipleconsumers.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafKaProducerService {

    /**
     *  Simple topic with a string payload
     */

    @Value(value = "${simple.topic.name}")
    private String simpleTopicName;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /**
     *  Topic with user object payload
     */

    @Value(value = "${user.topic.name}")
    private String userTopicName;

    private final KafkaTemplate<String, User> userKafkaTemplate;

    public void sendSimpleMessage(String message) {
        ListenableFuture<SendResult<String, String>> future
                = this.kafkaTemplate.send(simpleTopicName, message);

        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error("Unable to send message: [{}] ", message, ex);
            }

            @Override
            public void onSuccess(SendResult<String, String> result) {
                log.info("Sent message: [{}] with offset: [{}]", message, result.getRecordMetadata().offset());
            }
        });
    }

    public void sendUserMessage(User user) {
        ListenableFuture<SendResult<String, User>> future
                = this.userKafkaTemplate.send(userTopicName, user);

        future.addCallback(new ListenableFutureCallback<SendResult<String, User>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error("Unable to send message: [{}] ", user, ex);
            }

            @Override
            public void onSuccess(SendResult<String, User> result) {
                log.info("Sent message: [{}] with offset: [{}]", user, result.getRecordMetadata().offset());
            }
        });
    }
}
